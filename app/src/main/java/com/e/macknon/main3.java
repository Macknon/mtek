package com.e.macknon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class main3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        BottomNavigationView navigationView = findViewById(R.id.btm_nav);

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int id = menuItem.getItemId();
                if (R.id.home == id){
                    Home fragment = new Home();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.FrameLayout, fragment);
                    fragmentTransaction.addToBackStack(null).commit();
                }
                if (R.id.emergency == id){
                    startActivity(new Intent(main3.this, Pop_up.class));

                }
                if (R.id.notifications == id){

                    Notifications fragment = new Notifications();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.FrameLayout, fragment);
                    fragmentTransaction.addToBackStack(null).commit();
                }
                if (R.id.profile == id){
                    Profile fragment = new Profile();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.FrameLayout, fragment);
                    fragmentTransaction.addToBackStack(null).commit();
                }
                if (R.id.settings == id){
                    Settings fragment = new Settings();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.FrameLayout, fragment);
                    fragmentTransaction.addToBackStack(null).commit();
                }
                return true;
            }
        });

        navigationView.setSelectedItemId(R.id.home);
    }
}
