package com.e.macknon;


import com.e.macknon.iPingWService;
import com.e.macknon.ServerStatus;
import retrofit2.Call;
import android.util.Log;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class pingRepository {

    private iPingWService pingws;

    public pingRepository() {
        Retrofit repo = new Retrofit.Builder()
                .baseUrl("https://hillcroftinsurance.com:8445/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.pingws = repo.create(iPingWService.class);

    }

    public void getServerStatus() {
        this.pingws.getStatus().enqueue(new Callback<ServerStatus>() {
            @Override
            public void onResponse(Call<ServerStatus> call, Response<ServerStatus> response) {
                ServerStatus r = response.body();
                Log.e("IPING", r.getGroup());
            }

            @Override
            public void onFailure(Call<ServerStatus> call, Throwable t) {

            }
        });
    }
}
